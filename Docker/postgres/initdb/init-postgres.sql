CREATE TABLE users(
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(50) NOT NULL,
    username        VARCHAR(40) NOT NULL,
    email           VARCHAR(100) NOT NULL,
    street          VARCHAR(40) NOT NULL,
    suite           VARCHAR(20) NOT NULL,
    city            VARCHAR(30) NOT NULL,
    zipcode         VARCHAR(30) NOT NULL,
    lat             VARCHAR(10) NOT NULL,
    lng             VARCHAR(10) NOT NULL,
    phone           VARCHAR(30) NOT NULL,
    website         VARCHAR(50) NOT NULL,
    company_name    VARCHAR(50) NOT NULL,
    catchph         VARCHAR(50) NOT NULL,
    bs              VARCHAR(50) NOT NULL
);