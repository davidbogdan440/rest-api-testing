# Spring Boot REST endpoint testing

This project was a task that I've got from **Cubicfox** to test my coding skills. 


# General info

My task was to implement a REST endpoint testing software in Java. I could use any framework and technology, but the suggested database PostgreSQL was. 

## Dependencies

 - Spring Data JPA
 -  Lombok
 - Spring Web
 - Validation
 - Devtools
There was not used third-party dependency. 

## Features

 - The program can call the given endpoint (https://jsonplaceholder.typicode.com/users) and store the data what it returns, to a PostgreSQL database.
 - I used a filter for logging the requests and responses to a log.txt file . In addition this filter checks if it was a succesful http call or not. (It checks if the status is 200.) 
 - User's email validation. I achived this with the @Email annotation and a regular expression. 

