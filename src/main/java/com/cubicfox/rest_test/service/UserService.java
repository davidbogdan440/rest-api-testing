package com.cubicfox.rest_test.service;

import com.cubicfox.rest_test.entity.User;
import com.cubicfox.rest_test.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public void saveUserToDb(User user){
        userRepository.save(user);
    }

}
