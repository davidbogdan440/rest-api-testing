package com.cubicfox.rest_test.entity;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Geo {

    private String lat;
    private String lng;

}