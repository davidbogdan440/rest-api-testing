package com.cubicfox.rest_test.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class Company {

    @Column(name = "company_name")
    private String name;
    @Column(name = "catchph")
    private String catchPhrase;
    private String bs;

}
