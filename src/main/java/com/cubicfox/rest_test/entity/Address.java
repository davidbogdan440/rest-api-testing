package com.cubicfox.rest_test.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Data
@Embeddable
public class Address {

    private String street;
    private String suite;
    private String city;
    private String zipcode;
    @Embedded
    private Geo geo;

}
