package com.cubicfox.rest_test.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Order(2)
@Component
public class LoggingFilter implements Filter {

    private final static Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Initializing filter :{}", this);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        logger.info("Logging Request  {} : {}, Type: {}", req.getMethod(), req.getRequestURL(), req.getDispatcherType());
        chain.doFilter(request, response);
        logger.info("Logging Response : Content-Type: {}, Status: {}", res.getContentType(), res.getStatus());
        if(res.getStatus() == 200){
            logger.info("It was a successful http call.");
        }
    }

    @Override
    public void destroy() {
        logger.warn("Destructing filter :{}", this);
    }
}
