package com.cubicfox.rest_test.repository;

import com.cubicfox.rest_test.entity.User;;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface UserRepository extends JpaRepository<User, Long> {
}
