package com.cubicfox.rest_test.controller;


import com.cubicfox.rest_test.entity.User;
import com.cubicfox.rest_test.filter.LoggingFilter;
import com.cubicfox.rest_test.service.UserService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;
    private final RestTemplate restTemplate;

    private final Logger logger = LoggerFactory.getLogger(Logger.class);

    private final String API_URL = "https://jsonplaceholder.typicode.com/users";

    @GetMapping("/users")
    public User[] getUsers(){
        User[] users = restTemplate.getForObject(API_URL, User[].class);
        for(User user:users){
            userService.saveUserToDb(user);
        }
        return users;
    }
}
